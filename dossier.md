 ![bis_intro](images/visualdesigns/desktop/bis_intro.png "bis_intro")
 
##**DOSSIER** 
 ** Lars Follet & Victor Gouhie || 2MMPC-12**


#Briefing & Analyse 
Het hoofdstuk briefing omdat de eigenlijke briefing uit het hoofdstuk omschrijving.
 De analyse van deze briefing is heel belangrijk om de inhoud, 
 functionaliteit en uiterlijke te bepalen. 
 Hanteer de kennis uit WDAD1 en WDAD2 om dit hoofdstuk en volgend goed te voltooien.
 

#Functionele specificaties
* One Page Webapplication
  * Bookmark routes
  * of een webapplicatie bestaande uit verschillende pagina's
* Preloaders/Loaders
  * Om de app in te laden
  * Tijdens het inladen van JSON(P)
  * Lokale data bestanden
* De meeste inhoud wordt beheerd in data bastanden en dynamisch ingeladen
* Adaptive images, video's en sounds
* Google Maps integratie of gelijkaardig
  * Custom look-and-feel
* GEO-location
  * Toon de locatie van de gebruiker op een Map
  * Hou rekening met deze locatie in andere onderdelen van deze app
* Social Media Bookmarking (Open Graph)
* Animaties via SVG en/of Canvas
* Lokaal caching van data en bronbestanden (cache manifest)
* Gebruiker ervaart een interactief webapplicatie
* Gebruiker kan favoriete data lokaal bewaren
* Gebruiker kan de webapplicatie bookmarken in browser, bureaublad en als native app in het overzicht
* Automation verplicht!
  * Componenten worden via Bower toegevoegd in de components folder van de app folder
  * SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
  * CSS-bestanden worden met elkaar verbonden in één bestand en geminified
  * De JS code wordt automatisch nagekeken op syntax fouten JS-bestanden worden met elkaar verbonden in één bestand en geminified
  * De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
  * Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken

#Technische specificaties
##Frontend
* Core technologies: HTML5, CSS3 en JavaScript
* Template engines: Jade, Haml, Swig of Handlebars
* Storage: JSON bestanden, localstorage en/of IndexedDB
* Bibliotheken: jQuery, underscore.js, lodash.js, crossroads.js, js-signals, Hasher.js, ...
* Andere bibliotheken worden hierin aangevuld tijdens het semester!
* Uitzonderingen mogelijk betreffende JavaScript bibliotheken mogelijk mits toelating! 
 > Webapplicatie moet responsive zijn! Het responsive framework alsook alle andere bestanden moeten zelf geïmplementeerd worden.

##Backend
> Er is geen backend aanwezig in deze webapplicatie.

#Persona's 
![persona1](images/persona's/persona_1_int.png "persona1")  
![persona2](images/persona's/persona_2.png "persona2")      
![persona3](images/persona's/Persona_3_int.png "persona3")  


#Indeëenborden
![ideëenbord_1](images/ideeënborden/Ideeënbord_1.png "ideëenbord_1") 
![ideëenbord_2](images/ideeënborden/Ideëenbord_2.png "ideëenbord_2") 
![ideëenbord_3](images/ideeënborden/ideeënbord_3.png "ideëenbord_3") 
![ideëenbord_4](images/ideeënborden/Ideeënbord_4.png "ideëenbord_4") 

##Moodboard
![moodboard](images/ideeënborden/moodboard.png "moodboard") 


#Sitemap
![sitemap](images/Sitemap.png "sitemap") 

#Wireframes
####Mobile
![wireframes_mobile_login](images/wireframes/mobile/wireframes_mobile_login.png "wireframes_mobile_login") 
![wireframes_mobile_about](images/wireframes/mobile/wireframes_mobile_about.png "wireframes_mobile_about") 
![wireframes_mobile_beginscherm](images/wireframes/mobile/wireframes_mobile_beginscherm.png "wireframes_mobile_beginscherm") 
![wireframes_mobile_statistieken](images/wireframes/mobile/wireframes_mobile_statistieken.png "wireframes_mobile_statistieken") 
![wireframes_mobile_iconeninfo](images/wireframes/mobile/wireframes_mobile_iconeninfo.png "wireframes_mobile_iconeninfo") 
![wireframes_mobile_tip](images/wireframes/mobile/wireframes_mobile_tip.png "wireframes_mobile_tip") 
####Desktop
![bis_intro](images/wireframes/desktop/Wireframes_bis_intro.png "bis_intro") 
![intro](images/wireframes/desktop/Wireframes_intro.png "intro") 
![about](images/wireframes/desktop/Wireframes_about.png "about") 
![country](images/wireframes/desktop/Wireframes_country.png "country") 
![statistieken](images/wireframes/desktop/Wireframes_statistieken.png "statistieken") 
![statistieken_uitleg](images/wireframes/desktop/Wireframes_statistieken_uitleg.png "statistieken_uitleg") 
![tips](images/wireframes/desktop/Wireframes_tips.png "tips") 
#Style Tiles

 ![styletile_1](images/styletiles/styletile_1.png "styletile_1") 
 ![styletile_2](images/styletiles/styletile_2.png "styletile_2") 
 ![styletile_finaal](images/styletiles/styletile_finaal.png "styletile_finaal") 
> Het font hebben we toch moeten aanpassen omdat Helvetica geen goed font was om juist te gaan centreren.
 Het bracht teveel problemen met ons mee. We hebben het gewijzigd naar Roboto. 

#Visual designs
####Mobile

 Intro
 ![intro](images/visualdesigns/mobile/screendesigns_mobile_Home.png "intro") 
 About
 ![about](images/visualdesigns/mobile/screendesigns_mobile_About.png "about") 
  Country
 ![land](images/visualdesigns/mobile/screendesigns_mobile_Land.png "land") 
  Statistieken
 ![statistieken](images/visualdesigns/mobile/screendesigns_mobile_Statistieken_copy.png "statistieken") 
  Statistieken uitleg
 ![statistieken_uitleg](images/visualdesigns/mobile/screendesigns_mobile_Statistieken_uitleg.png "statistieken_uitleg") 
Tip
 ![tip](images/visualdesigns/mobile/screendesigns_mobile_Tips.png "tip") 
 
####Desktop
Bis intro
>animatie van het logo

 ![bis_intro](images/visualdesigns/desktop/bis_intro.png "bis_intro") 
 
 Intro
 ![intro](images/visualdesigns/desktop/intro.png "intro") 
 Intro hover + ingevulde form
 ![intro_hover](images/visualdesigns/desktop/intro_hover.png "intro_hover") 
  About
 ![about](images/visualdesigns/desktop/about.png "about") 
 About hover
 ![about_hover](images/visualdesigns/desktop/about_hover.png "about_hover") 
 Country 
 ![country](images/visualdesigns/desktop/country.png "country") 
  Country hover + ingevulde form
 ![country_hover](images/visualdesigns/desktop/country_hover.png "country_hover") 
  Statistieken
 ![Statistieken](images/visualdesigns/desktop/statistieken.png "Statistieken") 
 Statistieken + hover
 ![Statistieken_hover](images/visualdesigns/desktop/statistieken_hover.png "Statistieken_hover") 
 Iconen uitleg
 ![Iconen_uitleg](images/visualdesigns/desktop/uitleg_statistieken.png "Iconen_uitleg") 
 Iconen uitleg hover
 ![Iconen_uitleg_hover](images/visualdesigns/desktop/uitleg_statistieken_hover.png "Iconen_uitleg_hover") 
 Tip
 ![Tip](images/visualdesigns/desktop/tip.png "Tip") 
 Tip hover
 ![Tip_hover](images/visualdesigns/desktop/tip_hover.png "Tip_hover")
  Error 404 
  > achtergrond als .gif
  
 ![Error404 ](images/visualdesigns/desktop/error404.png "Error404 ") 
 
#Screenshots eindresultaat
 ![eindresultaat_home](images/screenshot/eindresultaat/screenshot_home.png "eindresultaat_home") 
 ![eindresultaat_about](images/screenshot/eindresultaat/screenshot_about.png "eindresultaat_about") 
 ![eindresultaat_country](images/screenshot/eindresultaat/screenshot_country.png "eindresultaat_country") 
 ![eindresultaat_result](images/screenshot/eindresultaat/screenshot_result.png "eindresultaat_result") 
 ![eindresultaat_logos](images/screenshot/eindresultaat/screenshot_logos.png "eindresultaat_logos") 
 ![eindresultaat_404](images/screenshot/eindresultaat/screenshot_404.png "eindresultaat_404") 
     

#Screenshots snippets 
####HTML
 ![html](images/screenshot/code/screenshot_html.png "html") 
####CSS
 ![css](images/screenshot/code/screenshot_css.png "css") 
####JavaScript
 ![JS](images/screenshot/code/screenshot_js1.png "js") 
####JavaScript
 ![JS](images/screenshot/code/screenshot_js2.png "js") 
####JavaScript
 ![JS](images/screenshot/code/screenshot_js3.png "js") 
####JavaScript
 ![JS](images/screenshot/code/screenshot_js4.png "js") 
 
#Tijdsbesteding per student
Beide partijen (Lars Follet & Victor Gouhie) gaan akkoord met de 50/50 regel. 










 ** Lars Follet & Victor Gouhie || 2MMPC-12** 
 
 ** Academiejaar 2015 - 2016 || Artevelde Hogeschool Gent || Grafische & Digitale Media - Produce**