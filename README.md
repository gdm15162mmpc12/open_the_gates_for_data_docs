 ![bis_intro](images/visualdesigns/desktop/bis_intro.png "bis_intro")
#OPEN THE GATES FOR DATA

##Synopsis applicatie 


** frank. berekent hoe groen jouw land is en geeft jou advies. 
Zorg er gewoon voor dat je niet begint te huilen. **

##Hoe het werkt:##

De gebruiker moet zijn/haar naam ingeven, frank. spreekt namelijk graag de gebruiker bij naam aan. 
Daarna kiest de gebruiker het land waar hij/zij op dat ogenblik woont. 
frank. doet al de rest en berekent aan de hand van verschillende datasets hoe groen dat specifiek land is. 
Dit gebeurt op basis van:

* Hoeveelheid CO2-uitstoot
* Verbuik van elektriciteit
* Waterverbuik
* Aantal rijdende wagens
* Hoeveelheid hernieuwbare energie
* Bosgebied

De gebruiker kiest daarna zelf wat hij/zij zou willen verbeteren aan zijn/haar land. 
De gebruiker drukt op 'fix this' en frank. geeft zijn advies. 
Soms kan dit advies heel streng máár rechtvaardig zijn. 
Probeer er mee te leven. 
De gebruiker kan dit advies delen met zijn/haar vrienden via sociale media en de app promoten.




####Gebruikte data: 

* [Hoeveelheid CO2-uitstoot](http://data.worldbank.org/indicator/EN.ATM.CO2E.PC)
* [Verbruik van elektriciteit](http://data.worldbank.org/indicator/EG.USE.ELEC.KH.PC ")
* [Waterverbruik](http://data.worldbank.org/indicator/ER.H2O.FWDM.ZS)
* [Aantal rijdende wagens](http://data.worldbank.org/indicator/IS.VEH.PCAR.P3)
* [Hoeveelheid hernieuwbare energie](http://data.worldbank.org/indicator/EG.FEC.RNEW.ZS)
* [Bosgebied](http://data.worldbank.org/indicator/AG.LND.FRST.K2)


 ![promotie](images/promotie.png "bis_intro")





 **Lars Follet & Victor Gouhie || 2MMPC-12**
 
 **Academiejaar 2015 - 2016 || Artevelde Hogeschool Gent || Grafische & Digitale Media - Produce** 
 
 
 



